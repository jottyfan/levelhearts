package com.firecontroller1847.levelhearts;

import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.firecontroller1847.levelhearts.gui.LevelHeartsHUD;

import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Mod(LevelHearts.MOD_ID)
@Mod.EventBusSubscriber(bus = Bus.MOD)
public class LevelHearts {

    public static final String NBT_ID = "levelHearts";
    public static final String MOD_ID = "levelhearts";
    public static final String MOD_NAME = "LevelHearts";
    public static final String MOD_VERSION = "1.1.1";
    public static final UUID MOD_UUID = UUID.fromString("81f27f52-c8bb-403a-a1a4-b356d2f7a0f0");
    public static AttributeModifier healthModifier;
    public static Logger logger = LogManager.getLogger(MOD_ID);

    public LevelHearts() {
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, LevelHeartsConfig.COMMON, "levelhearts.toml");
    }

    @SubscribeEvent
    public static void clientSetup(FMLClientSetupEvent event) {
        MinecraftForge.EVENT_BUS.register(new LevelHeartsHUD());
    }

    public static void debug(String message) {
        if (LevelHeartsConfig.GENERAL.debug.get()) {
            logger.info(message);
        }
    }

}
