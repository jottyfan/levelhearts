package com.firecontroller1847.levelhearts;

import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootEntry.Builder;
import net.minecraft.world.storage.loot.LootPool;
import net.minecraft.world.storage.loot.RandomValueRange;
import net.minecraft.world.storage.loot.TableLootEntry;
import net.minecraftforge.event.LootTableLoadEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = LevelHearts.MOD_ID)
public class LevelHeartsLootTables {

    @SubscribeEvent
    public static void registerLootTables(LootTableLoadEvent event) {
        
        String path = event.getName().getPath();
        LevelHearts.debug("LootTableLoad{Event}: " + path);
        if (path.equals("chests/abandoned_mineshaft")) {
            event.getTable().addPool(fetchLootPoolChests("lh_abandoned_mineshaft"));
        } else if (path.equals("chests/desert_pyramid")) {
            event.getTable().addPool(fetchLootPoolChests("lh_desert_pyramid"));
        } else if (path.equals("chests/jungle_temple")) {
            event.getTable().addPool(fetchLootPoolChests("lh_jungle_temple"));
        } else if (path.equals("chests/simple_dungeon")) {
            event.getTable().addPool(fetchLootPoolChests("lh_simple_dungeon"));
        } else if (path.equals("chests/stronghold_corridor")) {
            event.getTable().addPool(fetchLootPoolChests("lh_stronghold_corridor"));
        } else if (path.equals("chests/stronghold_crossing")) {
            event.getTable().addPool(fetchLootPoolChests("lh_stronghold_crossing"));
        } else if (path.equals("chests/stronghold_library")) {
            event.getTable().addPool(fetchLootPoolChests("lh_stronghold_library"));
        } else if (path.equals("chests/village/village_toolsmith")) {
            event.getTable().addPool(fetchLootPoolChests("lh_village_toolsmith", true));
        }
    }

    private static LootPool fetchLootPoolChests(String name) {
        return fetchLootPoolChests(name, false);
    }

    private static LootPool fetchLootPoolChests(String name, boolean village) {
        return fetchLootPool(LevelHearts.MOD_ID + ":chests", village ? "village/" + name : name,
                new RandomValueRange(1));
    }

    private static LootPool fetchLootPool(String location, String name, RandomValueRange rolls) {
        LevelHearts.debug("LootTableLoad{Event}: Adding new loot @ " + (location + "/" + name));
        
        Builder<?> entry = TableLootEntry.builder(new ResourceLocation(location + "/" + name)).quality(1).weight(1);
        LootPool pool = LootPool.builder().rolls(rolls).addEntry(entry).build();

        return pool;
    }

}
