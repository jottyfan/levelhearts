package com.firecontroller1847.levelhearts;

import com.firecontroller1847.levelhearts.item.ItemHeartContainer;
import com.firecontroller1847.levelhearts.item.ItemHeartPiece;

import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;

@Mod.EventBusSubscriber(modid = LevelHearts.MOD_ID, bus = Bus.MOD)
public class LevelHeartsItems {

    public static Item heartContainer = new ItemHeartContainer("heart_container");
    public static Item heartPiece = new ItemHeartPiece("heart_piece");

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll(heartContainer, heartPiece);
    }

}
