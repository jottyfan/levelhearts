package com.firecontroller1847.levelhearts;

import java.util.ArrayList;
import java.util.Arrays;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.Builder;
import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;

public class LevelHeartsConfig {
    private static final Builder BUILDER = new Builder();
    public static final General GENERAL = new General(BUILDER);
    public static final Health HEALTH = new Health(BUILDER);
    public static final Gui GUI = new Gui(BUILDER);
    public static final Xp XP = new Xp(BUILDER);
    public static final ForgeConfigSpec COMMON = BUILDER.build();

    public static class General {
        public final ConfigValue<Boolean> debug;
        public final ConfigValue<Boolean> hardcore;

        public General(Builder builder) {
            builder.push("General");

            debug = builder.comment(" Provides more output to the log. Mostly used for development and debugging.")
                    .define("Debug", false);
            hardcore = builder.comment(" Resets the player's progress after death.").define("Hardcore", false);

            builder.pop();
        }
    }

    public static class Health {
        // No byte builder; defined as Integer, used as Byte internally
        public final ConfigValue<Integer> defHealth;
        // No short builder; defined as Integer, used as Short internally
        public final ConfigValue<Integer> maxHealth;
        // No short builder; defined as Integer, used as Short internally
        public final ConfigValue<ArrayList<Integer>> levelRamp;

        public Health(Builder builder) {
            builder.push("Health");

            defHealth = builder.comment(" The amount of half-hearts a user will have in a new game and after death.")
                    .defineInRange("Default Health", 20, 1, Byte.MAX_VALUE);
            maxHealth = builder.comment(" The maximum amount of half-hearts a user can have.")
                    .defineInRange("Maximum Health", -1, -1, Short.MAX_VALUE);
            levelRamp = builder.comment(" The levels at which the user will gain a heart.").define("Level Ramp",
                    new ArrayList<Integer>(
                            Arrays.asList(1, 5, 10, 15, 20, 25, 30, 34, 38, 42, 46, 50, 53, 56, 59, 62, 64, 66, 68, 70,
                                    75, 80, 85, 90, 95, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200)));

            builder.pop();
        }
    }

    public static class Gui {
        public final ConfigValue<Boolean> hud;
        public final ConfigValue<Boolean> minimalHud;

        public Gui(Builder builder) {
            builder.push("Gui");

            hud = builder.comment(
                    " Whether or not to use the custom HUD provided by LevelHearts. Only disable if you're having mod conflicts.")
                    .define("LevelHearts HUD", true);
            minimalHud = builder.comment(" Changes the HUD to display health in one-row only.")
                    .define("LevelHearts Minimal HUD", false);

            builder.pop();
        }
    }

    public static class Xp {
        public final ConfigValue<Double> multiplier;
        public final ConfigValue<Boolean> loseOnDeath;

        public Xp(Builder builder) {
            builder.push("Xp");

            multiplier = builder.comment("How much to multiply the value of XP by.").defineInRange("XP Multiplier", 1.0,
                    0, Byte.MAX_VALUE);
            loseOnDeath = builder.comment("Force players to lose their XP even if keepInventory is enabled.")
                    .define("Always Lose XP on Death", false);

            builder.pop();
        }
    }

}
