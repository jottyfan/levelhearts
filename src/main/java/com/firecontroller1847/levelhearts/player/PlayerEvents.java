package com.firecontroller1847.levelhearts.player;

import java.util.ArrayList;

import com.firecontroller1847.levelhearts.LevelHearts;
import com.firecontroller1847.levelhearts.LevelHeartsConfig;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.AttributeModifier.Operation;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.event.TickEvent.PlayerTickEvent;
import net.minecraftforge.event.entity.living.LivingExperienceDropEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerChangedDimensionEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerLoggedOutEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerRespawnEvent;
import net.minecraftforge.event.entity.player.PlayerPickupXpEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = LevelHearts.MOD_ID)
public class PlayerEvents {
    private static int tickDelay = 0;

    @SubscribeEvent
    public static void onPlayerLogin(PlayerLoggedInEvent event) {
        LevelHearts.debug("PlayerLoggedIn{Event}");
        PlayerData data = new PlayerData().loadFromPlayer(event.getPlayer());

        LevelHearts.debug("PlayerLoggedIn{Event}: Searching for previous PlayerData...");
        if (data == null) {
            LevelHearts.debug("PlayerLoggedIn{Event}: No data found for player, creating new PlayerData");
            data = new PlayerData(false);
            data.saveToPlayer(event.getPlayer());
            setHealthModifier(event.getPlayer(), data.getModifierWithHeartContainers());
            event.getPlayer().setHealth(event.getPlayer().getMaxHealth());

            LevelHearts.debug("PlayerLoggedIn{Event}: PlayerData has been saved to the player " + data);
        } else {
            LevelHearts.debug("PlayerLoggedIn{Event}: Found previous PlayerData " + data);
            setHealthModifier(event.getPlayer(), data.getModifierWithHeartContainers());
        }

        data.hasRunLogin = true;
        PlayerData.cachePlayerData(event.getPlayer(), data);
    }

    @SubscribeEvent
    public static void onPlayerTick(PlayerTickEvent event) {
        // Only run every half-second. No need to run every tick.
        tickDelay++;
        if (tickDelay < 10) return;
        tickDelay = 0;

        PlayerData data = PlayerData.getCachedPlayerData(event.player);
        if (data != null && data.hasRunLogin && hasLevelChanged(event.player, data)) {
            LevelHearts.debug("PlayerTick{Event}: Player's level has changed!");
            ArrayList<Integer> ramp = LevelHeartsConfig.HEALTH.levelRamp.get();
            int max = LevelHeartsConfig.HEALTH.maxHealth.get();

            // Run loop if level ramp is less than ramp length, the player has enough
            // experience, and the player has not hit max health.
            while (data.levelRampPosition < ramp.size()
                    && event.player.experienceLevel >= ramp.get(data.levelRampPosition)
                    && (max <= 0 || event.player.getMaxHealth() < max)) {
                event.player.sendMessage(new TranslationTextComponent("text." + LevelHearts.MOD_ID + ".heartadded"));
                data.levelRampPosition++;
                data.modifier += 2;
                PlayerData.cachePlayerData(event.player, data);

                setHealthModifier(event.player, data.getModifierWithHeartContainers());
                event.player.setHealth(event.player.getMaxHealth());

                LevelHearts.debug("PlayerTick{Event}: XP matches next level ramp. Player's health has increased by 1");
            }
        }
    }

    @SubscribeEvent
    public static void onPlayerRespawn(PlayerRespawnEvent event) {
        LevelHearts.debug("PlayerRespawn{Event}");

        PlayerData data = PlayerData.getCachedPlayerData(event.getPlayer());
        if (LevelHeartsConfig.GENERAL.hardcore.get()) {
            data.heartContainers = 0;
            data.levelRampPosition = 0;
            data.modifier = LevelHeartsConfig.HEALTH.defHealth.get()
                    - SharedMonsterAttributes.MAX_HEALTH.getDefaultValue();
            PlayerData.cachePlayerData(event.getPlayer(), data);
        }
        if (LevelHeartsConfig.XP.loseOnDeath.get()) {
            event.getPlayer().addExperienceLevel(-(event.getPlayer().experienceLevel + 1));
        }

        setHealthModifier(event.getPlayer(), data.getModifierWithHeartContainers());
        event.getPlayer().setHealth(event.getPlayer().getMaxHealth());
    }

    @SubscribeEvent
    public static void onLivingExperienceDrop(LivingExperienceDropEvent event) {
        if (!LevelHeartsConfig.XP.loseOnDeath.get() || !(event.getEntity() instanceof PlayerEntity)) return;
        LevelHearts.debug("LivingExperienceDrop{Event}");

        LevelHearts.debug("LivingExperienceDrop{Event}: droppedExperience=" + event.getDroppedExperience());
        PlayerEntity player = (PlayerEntity) event.getEntity();

        int i = player.experienceLevel * 7;
        event.setDroppedExperience(i > 100 ? 100 : i);
        LevelHearts.debug("LivingExperienceDrop{Event}: newDroppedExperience=" + event.getDroppedExperience());
    }

    @SubscribeEvent
    public static void onPlayerChangedDimension(PlayerChangedDimensionEvent event) {
        LevelHearts.debug("PlayerChangedDimension{Event}");

        PlayerData data = PlayerData.getCachedPlayerData(event.getPlayer());
        setHealthModifier(event.getPlayer(), data.getModifierWithHeartContainers());
    }

    @SubscribeEvent
    public static void onPlayerPickupXp(PlayerPickupXpEvent event) {
        LevelHearts.debug("PlayerPickupXp{Event}");

        event.getOrb().xpValue *= LevelHeartsConfig.XP.multiplier.get();
    }

    @SubscribeEvent
    public static void onPlayerLogout(PlayerLoggedOutEvent event) {
        LevelHearts.debug("PlayerLoggedOut{Event}");

        PlayerData data = PlayerData.getCachedPlayerData(event.getPlayer());
        if (data != null) {
            data.saveToPlayer(event.getPlayer());

            LevelHearts.debug("PlayerLoggedOut{Event}: Saved PlayerData");
        }
        PlayerData.removeCachedPlayerData(event.getPlayer());
    }

    private static boolean hasLevelChanged(PlayerEntity player, PlayerData data) {
        boolean changed = false;
        if (data.cachedLevel != player.experienceLevel) {
            data.cachedLevel = player.experienceLevel;
            changed = true;
        }
        return changed;
    }

    public static void setHealthModifier(PlayerEntity player, double modifier) {
        LevelHearts.healthModifier = new AttributeModifier(LevelHearts.MOD_UUID, LevelHearts.NBT_ID + ".healthModifier",
                modifier, Operation.ADDITION);
        IAttributeInstance attribute = player.getAttribute(SharedMonsterAttributes.MAX_HEALTH);
        attribute.removeModifier(LevelHearts.healthModifier);
        attribute.applyModifier(LevelHearts.healthModifier);

        // Force a client-side update
        player.setHealth(player.getHealth() - 1);
        player.setHealth(player.getHealth() + 1);
    }
}
