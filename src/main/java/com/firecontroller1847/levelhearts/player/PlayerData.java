package com.firecontroller1847.levelhearts.player;

import java.util.concurrent.ConcurrentHashMap;

import com.firecontroller1847.levelhearts.LevelHearts;
import com.firecontroller1847.levelhearts.LevelHeartsConfig;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;

public class PlayerData {

    private static ConcurrentHashMap<String, PlayerData> datas = new ConcurrentHashMap<String, PlayerData>();
    protected boolean hasRunLogin = false;
    protected int cachedLevel;

    public byte heartContainers;
    protected byte defHealth;
    protected double modifier;
    protected short levelRampPosition;

    public PlayerData() {
        this(true);
    }

    public PlayerData(boolean empty) {
        if (empty) return;
        this.defHealth = LevelHeartsConfig.HEALTH.defHealth.get().byteValue();
        this.modifier = this.defHealth - SharedMonsterAttributes.MAX_HEALTH.getDefaultValue();
        this.heartContainers = 0;
        this.levelRampPosition = 0;
    }

    public double getModifierWithHeartContainers() {
        return (double) (this.modifier + this.heartContainers * 2);
    }

    public PlayerData loadFromPlayer(PlayerEntity player) {
        CompoundNBT entityTags = player.getPersistentData();
        if (!entityTags.contains(LevelHearts.NBT_ID)) return null;
        CompoundNBT tag = entityTags.getCompound(LevelHearts.NBT_ID);

        this.defHealth = tag.getByte("defHealth");
        this.modifier = tag.getDouble("modifier");
        this.heartContainers = tag.getByte("heartContainers");
        this.levelRampPosition = tag.getShort("levelRampPosition");
        return this;
    }

    public void saveToPlayer(PlayerEntity player) {
        CompoundNBT tag = new CompoundNBT();
        tag.putByte("minHealth", this.defHealth);
        tag.putDouble("modifier", this.modifier);
        tag.putByte("heartContainers", this.heartContainers);
        tag.putShort("levelRampPosition", this.levelRampPosition);
        player.getPersistentData().put(LevelHearts.NBT_ID, tag);
    }

    public static PlayerData getCachedPlayerData(PlayerEntity player) {
        return datas.get(PlayerEntity.getUUID(player.getGameProfile()).toString());
    }

    public static void removeCachedPlayerData(PlayerEntity player) {
        datas.remove(PlayerEntity.getUUID(player.getGameProfile()).toString());
    }

    public static void cachePlayerData(PlayerEntity player, PlayerData data) {
        datas.put(PlayerEntity.getUUID(player.getGameProfile()).toString(), data);
    }

    public String toString() {
        return "PlayerData{minHealth=" + this.defHealth + ", modifier=" + this.modifier + ", cachedLevel="
                + this.cachedLevel + "}";
    }

}
