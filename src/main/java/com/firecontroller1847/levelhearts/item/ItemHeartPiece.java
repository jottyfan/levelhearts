package com.firecontroller1847.levelhearts.item;

import com.firecontroller1847.levelhearts.LevelHearts;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;

public class ItemHeartPiece extends Item {

    public ItemHeartPiece(String name) {
        super(new Properties().maxStackSize(4).group(ItemGroup.MISC));
        this.setRegistryName(LevelHearts.MOD_ID, name);
    }

}
