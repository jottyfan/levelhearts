package com.firecontroller1847.levelhearts.item;

import com.firecontroller1847.levelhearts.LevelHearts;
import com.firecontroller1847.levelhearts.LevelHeartsConfig;
import com.firecontroller1847.levelhearts.LevelHeartsItems;
import com.firecontroller1847.levelhearts.player.PlayerData;
import com.firecontroller1847.levelhearts.player.PlayerEvents;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class ItemHeartContainer extends Item {

    public ItemHeartContainer(String name) {
        super(new Properties().maxStackSize(1).group(ItemGroup.MISC));
        this.setRegistryName(LevelHearts.MOD_ID, name);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, PlayerEntity player, Hand hand) {
        ItemStack stack = player.getHeldItem(hand);
        ActionResult<ItemStack> result = new ActionResult<ItemStack>(ActionResultType.PASS, stack);
        if (world.isRemote || player.abilities.disableDamage) return result;

        // Check if the item is a heart container
        if (!stack.getItem().equals(LevelHeartsItems.heartContainer)) return result;
        result = new ActionResult<ItemStack>(ActionResultType.FAIL, stack);

        // Get cached player data and fail if null
        PlayerData data = PlayerData.getCachedPlayerData(player);
        if (data == null) return result;

        // Set result to success as everything from here on is a success
        stack.setCount(stack.getCount() - 1);
        result = new ActionResult<ItemStack>(ActionResultType.SUCCESS, stack);

        // Check max health to determine if it's full; replenishes health instead if
        // full.
        LevelHearts.debug("HeartContainerRightClick{Event}");
        int max = LevelHeartsConfig.HEALTH.maxHealth.get();
        if (max > 0 && player.getMaxHealth() >= max) {
            player.sendMessage(new TranslationTextComponent("text." + LevelHearts.MOD_ID + ".replenished"));
            player.setHealth(player.getMaxHealth());

            LevelHearts.debug("HeartContainerRightClick{Event}: Player is at max health, replenished health.");
            return result;
        }

        // Add one to the heart containers and then update cache and health
        data.heartContainers += 1;
        PlayerData.cachePlayerData(player, data);
        PlayerEvents.setHealthModifier(player, data.getModifierWithHeartContainers());
        player.setHealth(player.getMaxHealth());
        player.sendMessage(new TranslationTextComponent("text." + LevelHearts.MOD_ID + ".heartadded"));

        LevelHearts.debug("HeartContainerRightClick{Event}: Added new heart and replenished health.");
        return result;
    }

}
